import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex); //registring VUEX into the vue instance

// central store and central state here: we need absolutely set with diferents names (actions, getters , setters,  mutations)
export const store = new Vuex.Store({
    state: { // must be called state to be detected by VUEX
        value: 0
    },
    getters,
    mutations,
    actions,
    modules: { // all the methods releated to changes counter state
        counter
    }
});