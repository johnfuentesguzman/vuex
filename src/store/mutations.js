import * as types from './types';

//  it does the redux reducers behaviour in Vuex: 
// MUST BE ALWAYS SINCRONOUS, due to in other case you never know what is mutation to do in case its used for multiple components 
export default {
    [types.MUTATE_UPDATE_VALUE]: (state, payload) => {
        state.value = payload;
    }
};