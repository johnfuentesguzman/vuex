import * as types from './types';

export default {
    [types.VALUE]: state => { // arrow function.. the getters must be created like that
        return state.value;
    }
};